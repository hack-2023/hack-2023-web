import { ComponentType } from 'react';
import { Route, Routes } from 'react-router';
import { TempPage } from 'pages/temp_page/TempPage';
import { LoginPage } from 'pages/login_page/LoginPage';
import { NotFoundPage } from 'pages/not_found/NotFoundPage';
import { Pages } from 'constants/links';
import Layout from '../components/layout';
import './App.css';

const wrapPageToLayout =
    (PageComponent: ComponentType) => (
        <Layout>
            <PageComponent />
        </Layout>
    );

function App() {
    return (
        <Routes>
            <Route path={Pages.Temp.url} element={wrapPageToLayout(TempPage)} />
            <Route path={Pages.Login.url} element={wrapPageToLayout(LoginPage)} />
            <Route path='*' element={wrapPageToLayout(NotFoundPage)} />
        </Routes>
    );
}

export default App;
