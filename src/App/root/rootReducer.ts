import { combineReducers, Reducer } from 'redux';
import app, { ActionsType as AppActionsType } from 'ducks/app';
import account, { ActionsType as AccountActionsType } from 'ducks/account';

// Чтобы добавить дргие редьюсеры допиши их через запятую.
// Например combineReducers({ app, example }).
const rootReducer = combineReducers({
    app,
    account,
});

export type RootState = ReturnType<typeof rootReducer>;

// Чтобы добавить ActionsType других редьюсеров используй union типы.
// Например AppActionsType | ExampleActionsType.
export type ActionsType = AppActionsType
    | AccountActionsType;

export default rootReducer as Reducer<RootState, ActionsType>;
