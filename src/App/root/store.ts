import { applyMiddleware, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { defaultState as AppDefaultState } from 'ducks/app/reducer';
import rootReducer, { RootState } from './rootReducer';
import rootSaga from './rootSaga';

const sagaMiddleware = createSagaMiddleware();
const enhancer = applyMiddleware(sagaMiddleware);

const cachedReduxState = JSON.parse(localStorage.getItem('app-state') ?? '{}') as RootState;

const store = createStore(rootReducer, cachedReduxState, enhancer);

store.subscribe(() => {
    localStorage.setItem('app-state', JSON.stringify(store.getState()));
});

window.addEventListener('beforeunload', () => {
    const stateOnBeforeUnload = store.getState();

    stateOnBeforeUnload.app = AppDefaultState;

    localStorage.setItem('app-state', JSON.stringify(stateOnBeforeUnload));
});

sagaMiddleware.run(rootSaga);

export default store;
