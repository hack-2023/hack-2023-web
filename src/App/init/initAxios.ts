import axios from 'axios';
import addCacheBusterInterceptors from 'api/interceptors/addCacheBusterInterceptors';
import addJwtInterceptors from 'api/interceptors/addJwtInterceptors';
import addNetworkErrorHandlerInterceptors from 'api/interceptors/addNetworkErrorInterceptors';

addCacheBusterInterceptors(axios);
addJwtInterceptors(axios);
addNetworkErrorHandlerInterceptors(axios);
