import { Reducer } from 'redux';
import * as actionTypes from './actionTypes';
import { ActionsType } from './types';

export interface IAppState {
    networkError?: Error;
    isLoading: boolean
}

export const defaultState: IAppState = {
    isLoading: false,
};

const reducer: Reducer<IAppState, ActionsType> = (state, action): IAppState => {
    state = state || defaultState;

    switch (action.type) {
        case actionTypes.SET_NETWORK_ERROR:
            return { ...state, networkError: action.payload };

        case actionTypes.SET_LOADING:
            return { ...state, isLoading: action.payload };

        default:
            return state;
    }
};

export default reducer;
