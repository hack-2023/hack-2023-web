import { SET_LOADING, SET_NETWORK_ERROR } from './actionTypes';

export const setNetworkError = (error: Error) => ({ type: SET_NETWORK_ERROR, payload: error } as const);

export const setLoading = (loading: boolean) => ({ type: SET_LOADING, payload: loading } as const);
