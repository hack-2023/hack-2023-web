import { IAccount } from 'api/types/v1.0/account';
import {
    SAVE_ACCOUNT,
    INIT_ACCOUNT,
    FETCH_ACCOUNT,
    LOGOUT,
    SET_LOADING,
    LOGIN,
} from './actionTypes';

export const initAccount = () => ({ type: INIT_ACCOUNT } as const);

export const saveAccount = (user: IAccount) => ({ type: SAVE_ACCOUNT, payload: user } as const);

export const fetchAccount = () => ({ type: FETCH_ACCOUNT } as const);

export const logout = () => ({ type: LOGOUT } as const);

export const login = (login: string, password: string) =>
    ({ type: LOGIN, payload: { login, password } } as const);

export const setAccountIsLoading = (isLoading: boolean) => ({ type: SET_LOADING, payload: isLoading } as const);
