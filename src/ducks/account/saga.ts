import { AxiosError, AxiosResponse } from 'axios';
import { takeLatest, put, call, all } from 'redux-saga/effects';
import { getAccount, logout as logoutFromServer, login as loginOnServer } from 'api/v1.0/account';
import { ErrorType, handleApiError } from 'api/handlers/errorHandlers';
import * as jwt from 'App/root/jwt';
import { IAccount } from 'api/types/v1.0/account';
import browserHistory from 'App/root/browserHistory';
import { Pages } from 'constants/links';
import { FETCH_ACCOUNT, INIT_ACCOUNT, LOGIN, LOGOUT } from './actionTypes';
import * as actionCreators from './actionCreators';

type ILoginAction = ReturnType<typeof actionCreators.login>;

function* initAccount() {
    yield put(actionCreators.setAccountIsLoading(true));
    try {
        const token = (yield call([jwt, 'get'])) as string | null;
        if (token) {
            yield put(actionCreators.fetchAccount());
        }
    } catch (exception) {
        yield call(handleApiError, exception as AxiosError<ErrorType>);
    } finally {
        yield put(actionCreators.setAccountIsLoading(false));
    }
}

export function* login({ payload }: ILoginAction) {
    yield put(actionCreators.setAccountIsLoading(true));
    try {
        const response = (yield call(loginOnServer, payload)) as AxiosResponse<string>;
        yield call([jwt, 'set'], response.data);
        yield put(actionCreators.fetchAccount());
        browserHistory.push(Pages.Temp.url);
    } catch (exception) {
        yield call(handleApiError, exception as AxiosError<ErrorType>);
    } finally {
        yield put(actionCreators.setAccountIsLoading(false));
    }
}

function* fetchAccount() {
    yield put(actionCreators.setAccountIsLoading(true));
    try {
        const response = (yield call(getAccount)) as AxiosResponse<IAccount>;
        yield put(actionCreators.saveAccount(response.data));
    } catch (exception) {
        yield call(handleApiError, exception as AxiosError<ErrorType>);
    } finally {
        yield put(actionCreators.setAccountIsLoading(false));
    }
}

function* logout() {
    yield put(actionCreators.setAccountIsLoading(true));
    try {
        yield call(logoutFromServer);
        yield call([jwt, 'clear']);
    } catch (exception) {
        yield call(handleApiError, exception as AxiosError<ErrorType>);
    } finally {
        yield put(actionCreators.setAccountIsLoading(false));
    }
}

export default function* watch() {
    yield all([
        takeLatest(INIT_ACCOUNT, initAccount),
        takeLatest(LOGIN, login),
        takeLatest(FETCH_ACCOUNT, fetchAccount),
        takeLatest(LOGOUT, logout),
    ]);
}
