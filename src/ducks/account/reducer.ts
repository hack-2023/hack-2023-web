import { Reducer } from 'redux';
import { IAccount } from 'api/types/v1.0/account';
import * as actionTypes from './actionTypes';
import { ActionsType } from './types';

export interface IState {
    user: IAccount | null;
    isLoading: boolean;
}

export const defaultState: IState = {
    user: null,
    isLoading: false,
};

const reducer: Reducer<IState, ActionsType> = (state, action) => {
    state = state || defaultState;

    switch (action.type) {
        case actionTypes.SAVE_ACCOUNT:
            return {
                ...state,
                user: action.payload,
            };

        case actionTypes.LOGOUT:
            return defaultState;

        case actionTypes.SET_LOADING:
            return {
                ...state,
                isLoading: action.payload,
            };

        default:
            return state;
    }
};

export default reducer;
