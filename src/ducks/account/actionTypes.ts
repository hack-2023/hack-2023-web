export const SAVE_ACCOUNT = 'account/SAVE_ACCOUNT' as const;
export const INIT_ACCOUNT = 'account/INIT_ACCOUNT' as const;
export const FETCH_ACCOUNT = 'account/FETCH_ACCOUNT' as const;
export const LOGIN = 'account/LOGIN' as const;
export const LOGOUT = 'account/LOGOUT' as const;
export const SET_LOADING = 'account/SET_LOADING' as const;
