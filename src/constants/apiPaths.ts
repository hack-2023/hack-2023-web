export const apiBaseUrl = process.env.API_BASE_URL ?? '/';

export const apiPaths = {
    // Получение токена.
    login: `${apiBaseUrl}/login`,

    // Отключение токена.
    tokenRefresh: `${apiBaseUrl}/token/refresh`,

    // Выход пользователя из системы.
    logout: `${apiBaseUrl}/logout`,

    // Профиль пользователя.
    userInfo: `${apiBaseUrl}/userInfo`,
};
