import './Spinner.css';

const Spinner: React.FC = () => {
    return (
        <span className={'loader'}></span>
    );
};

export default Spinner;
