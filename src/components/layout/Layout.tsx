import React from 'react';
import { useSelector } from 'react-redux';
import { RootState } from 'App/root/rootReducer';
import Spinner from '../spinner';
import LayoutHeader from './components/LayoutHeader';
import LayoutFooter from './components/LayoutFooter';
import './Layout.css';

interface IProps {
    children: React.ReactNode;
}

const Layout: React.FC<IProps> = ({ children }) => {
    const { isLoading } = useSelector((state: RootState) => state.app);

    return (
        <div className={'layout'}>
            <LayoutHeader />
            <div className={'layoutBody'}>
                {isLoading ? <Spinner /> : children}
            </div>
            <LayoutFooter />
        </div>
    );
};

export default Layout;
