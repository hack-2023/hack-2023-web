import React from 'react';
import { logout } from 'ducks/account/actionCreators';
import browserHistory from 'App/root/browserHistory';
import { Pages } from 'constants/links';
import store from 'App/root/store';

export const Logout: React.FC = () => {
    const logoutUser = () =>{
        store.dispatch(logout());
        browserHistory.push(Pages.Temp.url);
    };

    return (
        <button onClick={() => logoutUser()}>Выйти</button>
    );
};
