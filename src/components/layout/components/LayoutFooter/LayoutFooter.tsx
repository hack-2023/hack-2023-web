import React from 'react';
import './LayoutFooter.css';

const LayoutFooter: React.FC = () => {
    return (
        <div className={'layoutFooter'}>
            Подвал приложения
        </div>
    );
};

export default LayoutFooter;
