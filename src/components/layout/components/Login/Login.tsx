import React from 'react';
import browserHistory from 'App/root/browserHistory';
import { Pages } from 'constants/links';

export const Login: React.FC = () => {
    const loginUser = () =>{
        browserHistory.push(Pages.Login.url);
    };

    return (
        <button onClick={() => loginUser()}>Войти</button>
    );
};
