import React from 'react';
import { useSelector } from 'react-redux';
import { RootState } from 'App/root/rootReducer';
import * as jwt from 'App/root/jwt';
import './LayoutHeader.css';
import { Logout } from '../Logout';
import { Login } from '../Login';

const LayoutHeader: React.FC = () => {
    const { user } = useSelector((state: RootState) => state.account);
    const isLoggedIn = !!jwt.get() && !!user;

    return (
        <div className={'layoutHeader'}>
            <div>Шаблон приложения</div>
            {isLoggedIn
                ? (
                    <>
                        <div>{user.name}</div>
                        <Logout />
                    </>
                )
                : <Login />
            }
        </div>
    );
};

export default LayoutHeader;
