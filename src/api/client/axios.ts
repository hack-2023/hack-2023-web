import axios, { AxiosRequestConfig } from 'axios';

const buildPrimitiveRequestConfig = (config?: AxiosRequestConfig) => {
    config = config || {};
    const headers = config?.headers as Record<string, string> || {};

    return {
        ...config,
        headers: {
            ...headers,
            'content-type': 'application/json',
        },
    };
};

const buildFileRequest = (file: File) => {
    const formData = new FormData();
    formData.append('file', file, file.name);
    return formData;
};

export function postPrimitive<T>(url: string, data: number | string, config?: AxiosRequestConfig) {
    return axios.post<T>(url, JSON.stringify(data), buildPrimitiveRequestConfig(config));
}

export function putPrimitive<T>(url: string, data: number | string, config?: AxiosRequestConfig) {
    return axios.put<T>(url, JSON.stringify(data), buildPrimitiveRequestConfig(config));
}

export function postFile<T>(url: string, data: File, config?: AxiosRequestConfig) {
    return axios.post<T>(url, buildFileRequest(data), config);
}

export function putFile<T>(url: string, data: File, config?: AxiosRequestConfig) {
    return axios.put<T>(url, buildFileRequest(data), config);
}
