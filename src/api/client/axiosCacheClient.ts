import { setupCache, buildMemoryStorage } from 'axios-cache-interceptor';
import Axios from 'axios';

const axiosCacheClient = setupCache(Axios, {
    storage: buildMemoryStorage(
        false,
        15 * 60 * 1000,
        false
    ),
});

export default axiosCacheClient;
