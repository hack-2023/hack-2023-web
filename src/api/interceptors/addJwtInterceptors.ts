/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import axios, { AxiosError, AxiosResponse, AxiosInstance } from 'axios';
import { AdaptAxiosRequestConfig } from 'api/types/axios/axiosRequestConfig';
import { JWT_EXPIRED_HTTP_HEADER, JWT_ERROR_DESCRIPTION_HTTP_HEADER } from 'constants/jwt';
import { fetchAccount, logout } from 'ducks/account';
import store from 'App/root/store';
import * as jwt from 'App/root/jwt';
import { refreshToken } from '../v1.0/account';

const buildJWTAuthorizationHeader = (token: string) => `Bearer ${token}`;

const injectJWT = (config: AdaptAxiosRequestConfig) => {
    if (!config.headers.Authorization) {
        const token = jwt.get();
        if (token) {
            config.headers.Authorization = buildJWTAuthorizationHeader(token);
        }
    }

    return config;
};

const isUnauthorizedError = (error: AxiosError) => !!error.response && error.response.status === 401;

const isTokenExpiredError = (error: AxiosError) =>
    !!(error.response && error.response.headers[JWT_EXPIRED_HTTP_HEADER]);

const isTokenInvalid = (error: AxiosError) => {
    const description = error.response?.headers[JWT_ERROR_DESCRIPTION_HTTP_HEADER] as string | undefined;
    return description && description.indexOf('invalid_token') !== -1;
};

class UnauthorizedErrorHandler {
    private tokenRefreshingPromise: Promise<string> | null = null;

    constructor() {
        this.handle = this.handle.bind(this);
        this.handleTokenExpiration = this.handleTokenExpiration.bind(this);
        this.handleInvalidToken = this.handleInvalidToken.bind(this);
        this.refreshToken = this.refreshToken.bind(this);
        this.repeatRequest = this.repeatRequest.bind(this);
        this.handleFreshToken = this.handleFreshToken.bind(this);
    }

    public handle(error: AxiosError): Promise<AxiosResponse> {
        if (isTokenExpiredError(error) || !error.response) {
            return this.handleTokenExpiration(error);
        }

        if (isTokenInvalid(error)) {
            return this.handleInvalidToken(error);
        }

        return Promise.reject(error);
    }

    public async handleTokenExpiration(error: AxiosError): Promise<AxiosResponse> {
        try {
            const token = await this.refreshToken();
            return await this.repeatRequest(error.config as AdaptAxiosRequestConfig, token);
        } catch (exception) {
            if (exception instanceof AxiosError && isUnauthorizedError(exception)) {
                store.dispatch(logout());
            }

            throw exception;
        }
    }

    public handleInvalidToken(error: AxiosError): Promise<AxiosResponse> {
        store.dispatch(logout());
        return Promise.reject(error);
    }

    private refreshToken(): Promise<string> {
        if (!this.tokenRefreshingPromise) {
            const axiosInstance = axios.create();
            axiosInstance.interceptors.request.use(injectJWT);
            this.tokenRefreshingPromise = refreshToken().then(this.handleFreshToken.bind(this));
        }

        return this.tokenRefreshingPromise;
    }

    private handleFreshToken(response: AxiosResponse<string>): Promise<string> {
        const token = response.data;
        jwt.set(token);
        store.dispatch(fetchAccount());

        this.tokenRefreshingPromise = null;

        return Promise.resolve(token);
    }

    private repeatRequest(requestConfig: AdaptAxiosRequestConfig, token: string): Promise<AxiosResponse> {
        return axios.request({
            ...requestConfig,
            // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
            headers: {
                ...requestConfig.headers,
                Authorization: buildJWTAuthorizationHeader(token),
            },
        });
    }
}

const unauthorizedErrorHandler = new UnauthorizedErrorHandler();

export const handleJWTError = (error: AxiosError) => {
    if (isUnauthorizedError(error) || !error.response) {
        return unauthorizedErrorHandler.handle(error);
    }

    return Promise.reject(error);
};

const addJwtInterceptors = (axiosInstance: AxiosInstance) => {
    axiosInstance.interceptors.request.use(injectJWT);
    axiosInstance.interceptors.response.use(undefined, handleJWTError);
};

export default addJwtInterceptors;
