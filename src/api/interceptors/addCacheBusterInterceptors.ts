import { AxiosInstance } from 'axios';
import { AdaptAxiosRequestConfig } from 'api/types/axios/axiosRequestConfig';

// https://www.itworld.com/article/2693447/ajax-requests-not-executing-or-updating-in-internet-explorer-solution.html#:~:text=Internet%20Explorer%2C%20in%20its%20wisdom,cache%20expires%20on%20that%20object
const injectCacheBuster = (config: AdaptAxiosRequestConfig) => {
    if (config.method === 'GET' || config.method === 'get') {
        const params = (config.params as Record<string, unknown>) || {};
        config.params = {
            ...params,
            'cache-buster': Date.now(),
        };
    }

    return config;
};

const addCacheBusterInterceptors = (axiosInstance: AxiosInstance) => {
    axiosInstance.interceptors.request.use(injectCacheBuster);
};

export default addCacheBusterInterceptors;
