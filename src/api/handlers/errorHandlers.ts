import { AxiosError } from 'axios';
import {
    Forbidden,
    NotFound,
    Unauthorized,
} from 'constants/responseCodes';

interface IApiErrorHandler {
    (errorType: ErrorType, httpCode: number): boolean;
}

export enum ErrorType {
    PermissionDenied = 'PermissionDenied',
    FieldModificationIsNotAllowed = 'FieldModificationIsNotAllowed',
    SessionExpired = 'SessionExpired',
    MaxFailedAttemptsExceeded = 'MaxFailedAttemptsExceeded',
    BadLink = 'BadLink',
}

const generalMessages = {
    [Forbidden]: 'Доступ запрещён.',
    [NotFound]: 'Ресурс не найден.',
    [Unauthorized]: 'Ваша ссессия авторизации истекла.',
    default: 'При обработке Вашего запроса произошла ошибка.',
};

export const tryHandleApiError = (
    httpCode: number,
    message?: string
): boolean => {
    switch (httpCode) {
        case Forbidden:
        case NotFound:
        case Unauthorized:
            // eslint-disable-next-line
            console.error(generalMessages[httpCode]);
            break;

        default:
            // eslint-disable-next-line
            console.error(message || generalMessages.default);
            break;
    }

    return true;
};

export const handleApiError = (
    error: AxiosError<ErrorType>,
    errorHandler?: IApiErrorHandler,
    message?: string
): void => {
    if (!error.isAxiosError || !error.response) {
        throw error;
    }

    const { status, data } = error.response;
    if (errorHandler && errorHandler(data, status)) {
        return;
    }

    if (!tryHandleApiError(status, message)) {
        throw error;
    }
};
