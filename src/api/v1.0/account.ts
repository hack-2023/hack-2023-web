// import { apiPaths } from 'constants/apiPaths';
// import { axiosGet, axiosPost, axiosPostRequest } from 'api/client/axiosClient';

import { AxiosResponse } from 'axios';
import {
    ILoginRequest,
    IAccountResponse,
} from 'api/types/v1.0/account';
import * as jwt from 'App/root/jwt';

export const logout = async () => {
    await new Promise((resolve) => setTimeout(resolve, 2000));

    // await axiosGet(apiPaths.logout);
};

export const login = async (request: ILoginRequest): Promise<AxiosResponse<string>> => {
    await new Promise((resolve) => setTimeout(resolve, 2000));

    return {
        data: `token-${request.login}`,
    } as AxiosResponse;

    // return axiosPost<string>(apiPaths.login);
};

export const refreshToken = async (): Promise<AxiosResponse<string>> => {
    await new Promise((resolve) => setTimeout(resolve, 2000));
    const token = jwt.get();

    return {
        data: token,
    } as AxiosResponse;

    // return axiosPost<string>(apiPaths.tokenRefresh);
};

export const getAccount = async (): Promise<AxiosResponse<IAccountResponse>> => {
    await new Promise((resolve) => setTimeout(resolve, 2000));

    return {
        data: {
            id: '1',
            name: 'admin',
        },
    } as AxiosResponse;

    // return axiosPostRequest<ILoginRequest, IAccountResponse>(apiPaths.userInfo, request);
};
