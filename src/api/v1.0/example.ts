import { AxiosResponse } from 'axios';
import { IExampleRequest, IExampleResponse } from 'api/types/v1.0/example';

export const getExample = async (request: IExampleRequest): Promise<AxiosResponse<IExampleResponse>> => {
    await new Promise((resolve) => setTimeout(resolve, 1000));

    return {
        data: {
            text:request.allData
                ? 'Hello, World!\nThis is a common web application template. You can study it as a perfect app!\nGood luck ;D'
                : 'Hello, World!',
        },
    } as AxiosResponse;
};
