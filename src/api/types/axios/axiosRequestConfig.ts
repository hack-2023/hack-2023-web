import { AxiosRequestConfig, AxiosRequestHeaders } from 'axios';

export interface AdaptAxiosRequestConfig extends AxiosRequestConfig {
    headers: AxiosRequestHeaders
}
