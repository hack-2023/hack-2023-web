export interface IExampleRequest {
    allData: boolean;
}

export interface IExampleResponse {
    text: string;
}
