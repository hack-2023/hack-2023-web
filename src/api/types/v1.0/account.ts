export interface IAccount {
    id: string;
    name: string;
}

export interface ILoginRequest {
    login: string;
    password: string;
}

export interface IAccountResponse extends IAccount {}
