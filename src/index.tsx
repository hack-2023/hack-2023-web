import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';
import App from 'App/App';
import store from 'App/root/store';
import CustomBrowserRouter from 'utils/CustomBrowserRouter';
import browserHistory from 'App/root/browserHistory';
import './index.css';

const root = ReactDOM.createRoot(
    document.getElementById('root') as HTMLElement
);
root.render(
    <Provider store={store}>
        <CustomBrowserRouter history={browserHistory}>
            <App />
        </CustomBrowserRouter>
    </Provider>
);
