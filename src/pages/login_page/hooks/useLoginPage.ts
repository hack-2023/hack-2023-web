import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'App/root/rootReducer';
import { login as loginAction } from 'ducks/account';
import * as jwt from 'App/root/jwt';
import browserHistory from 'App/root/browserHistory';
import { Pages } from 'constants/links';

export const useLoginPage = () => {
    const account = useSelector((state: RootState) => state.account);
    const dispatch = useDispatch();
    const loginLoading = useSelector((state: RootState) => state.account.isLoading);

    const handleLogin = (login: string, password: string) => {
        dispatch(loginAction(login, password));
    };

    useEffect(() => {
        if (jwt.get() && account.user) {
            browserHistory.push(Pages.Temp.url);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return {
        loginLoading,
        handleLogin,
    };
};
