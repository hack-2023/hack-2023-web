import React, { useState } from 'react';
import Spinner from '../../components/spinner';
import { useLoginPage } from './hooks/useLoginPage';

export const LoginPage: React.FC = () => {
    const {
        loginLoading,
        handleLogin,
    } = useLoginPage();
    const [values, setValues] = useState<{ [key: string]: string }>({});

    const handleChangeInputs = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { target } = event;
        const { name, value } = target;

        setValues({
            [name]: value,
        });
    };

    const handleSubmitForm = (event: React.FormEvent) => {
        event.preventDefault();
        event.stopPropagation();

        handleLogin(values['login'], values['password']);
    };

    return (
        <div>
            {
                loginLoading
                    ? <Spinner />
                    : (
                        <form onSubmit={handleSubmitForm}>
                            <input type={'text'} name={'login'} onChange={handleChangeInputs} />
                            <input type={'password'} name={'password'} onChange={handleChangeInputs} />
                            <input type={'submit'} value={'Войти'} />
                        </form>
                    )
            }
        </div>
    );
};
