import React from 'react';
import Spinner from 'components/spinner';
import { useTempPage } from './hooks/useTempPage';
import './TempPage.css';

export const TempPage: React.FC = () => {
    const {
        textLoading,
        pageText,
        loadAllData,
        setLoadAllData,
    } = useTempPage();

    const handleCheckboxChange = () => {
        setLoadAllData((checked) => !checked);
    };

    return (
        <div className={'tempPage'}>
            <h2>{textLoading ? <Spinner /> : pageText}</h2>
            <label>
                Load all text
                <input
                    type={'checkbox'}
                    defaultChecked={loadAllData}
                    onChange={handleCheckboxChange}
                />
            </label>
        </div>
    );
};
