import { useCallback, useEffect, useState } from 'react';
import { getExample } from 'api/v1.0/example';

export const useTempPage = () => {
    const [pageText, setPageText] = useState<string>();
    const [textLoading, setTextLoading] = useState(false);
    const [loadAllData, setLoadAllData] = useState(true);

    const refreshText = useCallback(() => {
        let isLastFetch = true;

        setTextLoading(true);
        getExample({ allData: loadAllData })
            .then((res) => isLastFetch ? setPageText(res.data.text) : null)
            .finally(() => isLastFetch ? setTextLoading(false) : null);

        return () => {
            isLastFetch = false;
        };
    }, [loadAllData]);

    useEffect(() => {
        return refreshText();
    }, [refreshText]);

    return {
        textLoading,
        pageText,
        loadAllData,
        setLoadAllData,
        refreshText,
    };
};
