import React from 'react';

import './NotFoundPage.css';

interface Props {
}

export const NotFoundPage: React.FC<Props> = () => {
    return (
        <div className={'not_found_page'}>
            <h1>Страница не найдена</h1>
        </div>
    );
};
